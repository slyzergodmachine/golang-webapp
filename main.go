package main

import (
	"encoding/json"
	"net/http"
	"os"
)

func main() {
	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			w.Write([]byte("Hello, Docker! <3"))
			return
		}
		w.WriteHeader(http.StatusNotImplemented)
	})
	mux.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			json.NewEncoder(w).Encode(struct{ Status string }{Status: "OK"})
			return
		}
		w.WriteHeader(http.StatusNotImplemented)
	})
	http.ListenAndServe(":" + httpPort, mux)
}